import unittest
import putzfish


class TestLabelOperations(unittest.TestCase):
    def test_add_labels(self):
        base_labels = ['A', 'B', 'C']
        additional_labels = ['D', 'E']
        labels = putzfish.add_labels(base_labels, additional_labels)
        self.assertCountEqual(labels, ['A', 'B', 'C', 'D', 'E'])

    def test_remove_labels(self):
        base_labels = ['A', 'B', 'C']
        remove_labels = ['A', 'B']
        labels = putzfish.remove_labels(base_labels, remove_labels)
        self.assertCountEqual(labels, ['C'])

    def test_add_label_when_label_exists(self):
        base_labels = ['A', 'B', 'C']
        label = 'A'
        labels = putzfish.add_label(base_labels, label)
        self.assertCountEqual(labels, ['A', 'B', 'C'])

    def test_add_label_when_label_does_not_exist(self):
        base_labels = ['A', 'B', 'C']
        label = 'F'
        labels = putzfish.add_label(base_labels, label)
        self.assertCountEqual(labels, ['A', 'B', 'C', 'F'])

    def test_remove_label_when_label_exists(self):
        base_labels = ['A', 'B', 'C']
        label = 'A'
        labels = putzfish.remove_label(base_labels, label)
        self.assertCountEqual(labels, ['B', 'C'])

    def test_remove_label_when_label_does_not_exist(self):
        base_labels = ['A', 'B', 'C']
        label = 'F'
        labels = putzfish.remove_label(base_labels, label)
        self.assertCountEqual(labels, ['A', 'B', 'C'])