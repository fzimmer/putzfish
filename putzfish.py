#!/usr/bin/env python3

import gitlab
import os
import sys
import logging
from logging.handlers import TimedRotatingFileHandler

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s [%(levelname)-5.5s] %(message)s',
                    handlers=[logging.StreamHandler(sys.stdout)])

GITLAB_EE = '278964'
GEO_STANDARD_LABELS = [
    'devops::enablement', 'group::geo', 'Enterprise Edition'
]
GEO_DEPRECATED_LABELS = [
    'Geo [DEPRECATED]', 'Geo : Automatic Verification', 'Geo : Kubernetes',
    'Geo : Replication Lag Warning', 'Geo : Selective Sync', 'Geo DR',
    'GEO GA', 'Geo Next Gen', 'Geo Performance', 'Geo Replication/Sync',
    'Geo Verification/Accuracy'
]
GEO_DR = 'category::disaster recovery'
GEO_REPLICATION = 'category::geo replication'
GEO_BACKUP = 'category:: backup and restore'


def add_labels(labels, additional_labels=[]):
    return list(set(labels + additional_labels))


def remove_labels(labels, remove_labels=[]):
    return list(set(labels) - set(remove_labels))


def add_label(labels, label):
    if label not in labels:
        labels.append(label)
    return labels


def remove_label(labels, label):
    if label in labels:
        labels.remove(label)
    return labels


def replace_label(labels, old, new):
    if old in labels:
        labels.remove(old).append(new)
    return labels


def connect_gitlab(token=os.environ['GITLAB_API_TOKEN']):
    return gitlab.Gitlab('https://gitlab.com', private_token=token)


def get_open_issues_by_label_in_project(conn,
                                        project_id,
                                        label,
                                        get_all=False):
    project = conn.projects.get(project_id)
    issues = project.issues.list(state='opened', labels=[label], all=get_all)
    return issues


def label_statistics(conn, labels, project_id):
    for label in labels:
        labeled_issues = get_open_issues_by_label_in_project(conn,
                                                             project_id,
                                                             label,
                                                             get_all=True)
        print(label, len(labeled_issues))


def cleanup_labels(conn, project_id, label, additional_labels):
    issues = get_open_issues_by_label_in_project(conn,
                                                 project_id,
                                                 label,
                                                 get_all=True)
    logging.info('Found ' + str(len(issues)) + ' open issues with label ' +
                 label)
    for issue in issues:
        issue_labels = list.copy(issue.labels)
        issue_labels = add_labels(issue_labels, additional_labels)
        issue_labels = remove_label(issue_labels, label)
        issue.labels = issue_labels
        issue.save()
        logging.info('Removed label ' + label + ' from ' + issue.web_url)
        logging.info('Added labels ' + ', '.join(additional_labels) + ' to ' +
                     issue.web_url)


def main():
    conn = connect_gitlab()
    cleanup_labels(conn, GITLAB_EE, 'Geo [DEPRECATED]', GEO_STANDARD_LABELS)
    label_statistics(conn, GEO_DEPRECATED_LABELS, GITLAB_EE)


if __name__ == "__main__":
    main()